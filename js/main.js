// ===== Scroll to Top ==== 
$(window).scroll(function() {
  if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200);    // Fade in the arrow
  } else {
      $('#return-to-top').fadeOut(200);   // Else fade out the arrow
  }
});
$('#return-to-top').click(function() {      // When arrow is clicked
  $('body,html').animate({
      scrollTop : 0                       // Scroll to top of body
  }, 500);
});


$(document).ready(function(){
  $('.feedbacks').slick(
    {
      dots: false,
      prevArrow: false,
      nextArrow: false
  }
  );
});

$('#mobile_menu').click(function() {      // When arrow is clicked
  $('#main_menu').toggleClass('mobile_hidden');
});

$('#read_more').click(function(event) {
  $(this).parent().find('#details-content').toggleClass("hidden");
  $(this).parent().find('#read_more').toggleClass("hidden");
})
$('#read_less').click(function(event) {
  $(this).parent().parent().find('#details-content').toggleClass("hidden");
  $(this).parent().parent().find('#read_more').toggleClass("hidden");
})
window.onscroll = function() {myFunction()};

var header = document.getElementById("main_menu");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky-header");
  } else {
    header.classList.remove("sticky-header");
  }
}
